package toggl

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/armanvp-lib/datatypes/date"
)

func TestToggl_GetTimeEntries(t *testing.T) {
	r := require.New(t)

	tk := Token(os.Getenv("TOGGL_TOKEN"))
	dt, err := date.New(time.Now().Format(date.DateFormat))
	r.NoError(err, "new date should return no error")

	testCases := map[string]struct {
		config Config
		from   date.Date
		to     date.Date
		err    error
	}{
		"invalid token": {
			config: Config{
				Token: "abcdef",
			},
			from: dt,
			to:   dt,
			err:  fmt.Errorf("unexpected status code: 403"),
		},
		"invalid from": {
			config: Config{
				Token: tk,
			},
			from: date.Date{},
			to:   dt,
			err:  fmt.Errorf("unexpected status code: 400"),
		},
		"invalid to": {
			config: Config{
				Token: tk,
			},
			from: dt,
			to:   date.Date{},
			err:  fmt.Errorf("unexpected status code: 400"),
		},
		"valid": {
			config: Config{
				Token: tk,
			},
			from: dt,
			to:   dt,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			toggl, err := New(tc.config)
			r.NoError(err, "new toggl should not return an error")
			r.NotNil(toggl, "new toggl should not be nil")

			te, err := toggl.GetTimeEntries(tc.from, tc.to)
			if tc.err != nil {
				r.Equal(tc.err.Error(), err.Error(), "get time entries should have the expected error")
				return
			}
			r.NoError(err, "get time entries should not return an error")
			r.NotNil(te, "time entries should not be nil")
		})
	}

}
