package toggl

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/armanvp-lib/datatypes/date"
)

const (
	endpoint = "https://api.track.toggl.com/api/v9"
)

type Toggl struct {
	config Config
}

type TimeEntry struct {
	ID              int       `json:"id"`
	WorkspaceID     int       `json:"workspace_id"`
	ProjectID       int       `json:"project_id"`
	TaskID          int       `json:"task_id"`
	Billable        bool      `json:"billable"`
	Start           time.Time `json:"start"`
	Stop            time.Time `json:"stop"`
	Duration        int       `json:"duration"`
	Description     string    `json:"description"`
	Tags            []string  `json:"tags"`
	TagsID          []int     `json:"tags_ids"`
	Duronly         bool      `json:"duronly"`
	At              time.Time `json:"at"`
	ServerDeletedAt time.Time `json:"server_deleted_at"`
	UserID          int       `json:"user_id"`
	UID             int       `json:"uid"`
	WID             int       `json:"wid"`
}

func New(c Config) (t *Toggl, err error) {
	if err = c.Validate(); err != nil {
		return
	}

	t = &Toggl{
		config: c,
	}

	return
}

func (t *Toggl) GetTimeEntries(from, to date.Date) (te []TimeEntry, err error) {
	url := fmt.Sprintf("%s/me/time_entries?start_date=%s&end_date=%s",
		endpoint,
		from,
		to,
	)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("error initialising request: %w", err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Basic %s", t.config.Token.Base64()))

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error fetching URL: %w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Printf("error closing body: %v", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	bs, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response: %w", err)
	}

	err = json.Unmarshal(bs, &te)
	if err != nil {
		return nil, fmt.Errorf("error parsing response: %w", err)
	}

	return
}
