package toggl

import (
	"encoding/base64"
	"fmt"
)

type Token string

func (t Token) Base64() (s string) {
	return base64.StdEncoding.EncodeToString([]byte(t + ":api_token"))
}

type Config struct {
	Token Token
}

func (c Config) Validate() (err error) {
	if c.Token == "" {
		return fmt.Errorf("missing token")
	}
	return
}
