package toggl

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestConfig_Validate(t *testing.T) {
	r := require.New(t)

	testCases := map[string]struct {
		config Config
		err    error
	}{
		"missing token": {
			err: fmt.Errorf("missing token"),
		},
		"valid": {
			config: Config{
				Token: "abcdef",
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			r.Equal(tc.err, tc.config.Validate(), "should have expected validation result")
		})
	}
}

func TestToken_Base64(t *testing.T) {
	r := require.New(t)

	token := Token("AbCdEf54321")
	r.Equal("QWJDZEVmNTQzMjE6YXBpX3Rva2Vu", token.Base64(), "should have expected encoded value")
}
